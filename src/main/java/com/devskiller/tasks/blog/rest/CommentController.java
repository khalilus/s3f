package com.devskiller.tasks.blog.rest;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.devskiller.tasks.blog.model.dto.NewCommentDto;
import com.devskiller.tasks.blog.model.dto.CommentDto;
import com.devskiller.tasks.blog.service.CommentService;

import java.util.List;

@Controller
@RestController
public class CommentController {

    private final CommentService commentService;


    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @RequestMapping(value = "/posts/{id}/comments", method = RequestMethod.POST)
    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Long addComment(@PathVariable Long id, @RequestBody NewCommentDto newCommentDto) {
        newCommentDto.setPostId(id);
        return commentService.addComment(newCommentDto);
    }

    @RequestMapping(value = "/posts/{id}/comments")
    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<CommentDto> getCommentsForPost(@PathVariable Long id) {
        return commentService.getCommentsForPost(id);
    }
}
